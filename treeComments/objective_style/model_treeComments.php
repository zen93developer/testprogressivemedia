<?php 
	class Model_TreeComments
	{
		private static $databaseLink;
		
		// функция подключения к базе данных
		public function get_connect()
		{
			self::$databaseLink = mysqli_connect('mysql.zzz.com.ua', 'Shagfey', 'Shagfey', 'shagfey') or die('Нет соединения с базой данных');
		}
		
		// функция подключения общего количества комментариев в базе данных
		public function get_count_treeComments()
		{
			$result = mysqli_query(self::$databaseLink, "SELECT COUNT(*) FROM treeComments");
			if(mysqli_num_rows($result) > 0)
			{
				return mysqli_fetch_array($result)[0];
			}
		}
		
		// данные о первых комментариях (без родителей)
		public function get_start_comments($parent_id)
		{
			$result = mysqli_query(self::$databaseLink, "SELECT * FROM treeComments WHERE parent_id = $parent_id ORDER BY datetime ASC");
			if(mysqli_num_rows($result) > 0)
			{
				$start_comments_array = array();
				while($row = mysqli_fetch_array($result))
				{
					array_push($start_comments_array, $row);
				}
				return $start_comments_array;
			}
		}
		
		// добавляем новый комментарий
		public function add_new_comment($comment, $parent_id)
		{
			mysqli_query(self::$databaseLink, "INSERT INTO treeComments VALUES(NULL, '$comment', 'test', NOW(), 0, 0, $parent_id)");
		}
		
		// узнаём сколько потомков у комментариях
		public function get_new_child_count($parent_id)
		{
			$result = mysqli_query(self::$databaseLink, "SELECT * FROM treeComments WHERE id = $parent_id");
			if(mysqli_num_rows($result) > 0)
			{
				$new_child_count = mysqli_fetch_array($result)['child_count'] + 1;
			}
			return $new_child_count;
		}
		
		// обновляем количество потомков
		public function set_child_count($count_child, $id)
		{
			mysqli_query(self::$databaseLink, "UPDATE treeComments SET child_count = $count_child WHERE id = $id");
		}
	}
?>