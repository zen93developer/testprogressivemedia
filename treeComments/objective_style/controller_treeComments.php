<?php 
	require_once('model_treeComments.php');

	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		$controller = new Controller_TreeComments;
		$controller->add_new_comment($_POST['comment'], $_POST['parent_id']);	
	}
	
	class Controller_TreeComments
	{
		private $model;
		private $view;
		
		public function __construct()
		{
			$this->model = new Model_TreeComments;
			$this->model->get_connect();
			$this->view = 'view_treeComments';
		}
		
		public function build_treeComments()
		{
			if(!file_exists('count.txt') and !file_exists('cash.html'))
			{
				$file_count = fopen('cash_count.txt', 'w');
				fwrite($file_count, $this->model->get_count_treeComments());
				fclose($file_count);
				$this->create_cash_comments();
				$this->generate(file_get_contents('cash_comments.html'));
			}
			else
			{
				if((string)$this->model->get_count_treeComments() == file_get_contents('cash_count.txt'))
				{
					$this->generate(file_get_contents('cash_comments.html'));
				}
				else
				{
					$this->create_cash_comments();
					$this->generate(file_get_contents('cash_comments.html'));
				}
			}
		}
		
		private function create_cash_comments()
		{
			ob_start();
			$start_comments = $this->model->get_start_comments(0);
			$this->get_comments($start_comments);
			$cash_comments = ob_get_contents();
			ob_get_clean();
			$cash_comments_document = fopen('cash_comments.html', 'w');
			fwrite($cash_comments_document, $cash_comments);
			fclose($cash_comments_document);
		}
		
		private function generate($data)
		{
			include('view_treeComments.php');
		}
		
		public function add_new_comment($comment, $parent_id)
		{
			$this->model->add_new_comment($comment, $parent_id);
			$new_child_count = $this->model->get_new_child_count($parent_id);
			$this->model->set_child_count($new_child_count, $parent_id);
		}
		
		private function get_comments($array)
		{
			for($i = 0; $i < count($array); $i++)
			{
				echo '<p class="fio"> ФИО отправителя: '.$array[$i]['fio'].';</p>'; 
				echo '<p class="datetime">Дата и время отправления: '.$array[$i]['datetime'].'</p>';
				echo '<p> Текст комментария: <br/><p class="comment">'.$array[$i]['comment'].'</p></p>';
				echo '
					<form method="POST" action="'.$_SERVER['PHP_SELF'].'">
						<input type="text" name="comment" />
						<input type="hidden" name="parent_id" value="'.$array[$i]['id'].'" />
						<input type="submit" name="add" value="Ответить" />
					</form>
				';
				if($array[$i]['child_count'] > 0)
				{
					$new_array = $this->model->get_start_comments($array[$i]['id']);
					$this->get_comments($new_array);
				}
			}
		}
	}
?>