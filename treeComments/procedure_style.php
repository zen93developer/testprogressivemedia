<?php 
	$link = mysqli_connect("localhost", "root", "", "progressiveMediaTest") or die("No connection with database");
	if($_POST['add'])
	{
		mysqli_query($link, "INSERT INTO treeComments VALUES(NULL, '$_POST[comment]', '', NOW(), 0, 0, $_POST[parent_id])");
		$result = mysqli_query($link, "SELECT * FROM treeComments WHERE id = $_POST[parent_id]");
		$new_child_count = mysqli_fetch_array($result)['child_count'] + 1;
		mysqli_query($link, "UPDATE treeComments SET child_count = $new_child_count WHERE id = $_POST[parent_id]");
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html lang="ru-RU">
	<head>
		<meta charset="utf-8" />
		<title>Дерево комментариев</title>
	</head>
	<body>
		<?php
		
			// 1. Берём данные из базы данных о количестве постов
			// 2. Сравниваем данные из кэша и выборку
			// 3. Если они равны и существует файл кеша комментариев, то вы водим его
			// 4. Иначе запускаем буфер
			// 5. Передаём все комментарии из базы данных в буфер
			// 6. Перезаписываем файл кэша новыми данными
			// 7. Открываем файл кэша
		
			$result = mysqli_query($link, "SELECT * FROM treeComments");
			$count_comments = 0;
			while($row = mysqli_fetch_array($result))
			{
				$count_comments++;
			}
			
			if(!file_exists('count.txt') and !file_exists('cash.html'))
			{
				$file = fopen('count.txt', 'w');
				fwrite($file, $count_comments);
				fclose($file);
				
				ob_start();
				$result = mysqli_query($link, "SELECT * FROM treeComments WHERE parent_id = 0");
				get_comments($result, $link);
				$cash = ob_get_contents();
				ob_end_clean();
				$file = fopen('cash.html', 'w');
				fwrite($file, $cash);
				fclose($file);
				echo file_get_contents('cash.html');
			}
			else 
			{
				if((string)$count_comments == file_get_contents('count.txt'))
				{
					echo file_get_contents('cash.html');
				}
				else
				{
					ob_start();
					$result = mysqli_query($link, "SELECT * FROM treeComments WHERE parent_id = 0");
					get_comments($result, $link);
					$cash = ob_get_contents();
					ob_end_clean();
					$file = fopen('cash.html', 'w');
					fwrite($file, $cash);
					fclose($file);
					echo file_get_contents('cash.html');
				}
			}
			
			function get_comments($query_object, $link)
			{
				$new_tree = array();
				if($query_object == false) exit();
				while($row = mysqli_fetch_array($query_object))
				{
					array_push($new_tree, $row);
				}
				for($i = 0 ; $i < count($new_tree); $i++)
				{
					echo '<p>'.$new_tree[$i]['comment'].'</p>';
					echo '
						<form method="POST" action="index.php">
							<input type="text" name="comment" />
							<input type="hidden" name="parent_id" value="'.$new_tree[$i]['id'].'" />
							<input type="submit" name="add" value="Добавить" />
						</form>
					';
					if($new_tree[$i]['child_count'] > 0)
					{
						$new_result = mysqli_query($link, "SELECT * FROM treeComments WHERE parent_id = ".$new_tree[$i]['id']);
						get_comments($new_result, $link);
					}
				}
			}
		?>
	</body>
</html>